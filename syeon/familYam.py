#-*- coding: utf-8 -*-
#!/usr/bin/python


import os
from os import path
import gi 
from gi.repository import GObject, Gtk, GdkPixbuf, Gdk, Pango

#picamera 
import time
import picamera

#thread
import threading

#web post
import requests

#nowtime
import datetime

#json
import json
import requests
import urllib

import sys
import subprocess


flag = False #flag가 ture가 되면 녹화 종료 
loop = 1
Min = 0

# parent video url 
url_parentV = 'https://test-api.kidsyam.com:3443/test/getListKidsYam?Creator=parent&Type=V'
u1 = urllib.urlopen(url_parentV)
video = ""

# kid video url
url_kidV  = 'https://test-api.kidsyam.com:3443/test/getListKidsYam?Creator=kid&Type=V'
u2 = urllib.urlopen(url_kidV)
Kvideo = ""

# parent Audio url
url_book = 'https://test-api.kidsyam.com:3443/test/getListKidsYam?Creator=parent&Type=A'
audio = ""

headers = {'AuthKey': 'c60690cc716b8ed0b8feb8333a59e98c'}

pxbf_default =""


pxbf_null = ""
pxbf_kid = ""
space = "                         "

font = Pango.FontDescription('Latha 15')

class MyWindow(Gtk.Window, threading.Thread):

	def __init__(self):
		global video
		global Kvideo
		global audio
		global pxbf_null
		global pxbf_kid
		global pxbf_default


		Gtk.Window.__init__(self, title="")


		self.set_border_width(0)
		#self.set_size_request(100,100)
		self.fullscreen()
		self.notebook = Gtk.Notebook()
		self.add(self.notebook)
	

		########################################### Message : 부모와 아이의 동영상을 리스트 형식으로 보여줌 #####################################################

		# 부모님 동영상 정보, 아이 동영상 정보 가져옴
		video= json.loads(u1.read())
		Kvideo= json.loads(u2.read())

		#creating the liststore 
		self.liststore1 = Gtk.ListStore(int,GdkPixbuf.Pixbuf, str, GdkPixbuf.Pixbuf, str)

		pics_list=[]
		pics_name=[]

		# get image
		for root, dirs, files in os.walk('./image/'):
			for fn in files:
				pics_name.append(fn)
				f= os.path.join(root, fn)
				pics_list.append(f)
		
		# image파일 내부의 사진을 각각 해당하는 pixbuf에 넣음 
		for name, pic in zip (pics_name,pics_list):
			if name =="null.jpg":
				pxbf_null = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic,1,1,True)
			elif name == "01.png":
				pxbf_message = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic, 100, 90, True)
			elif name == "02.png":
				pxbf_kidsyam = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic,  100, 90, True)
			elif name == "03.png":
				pxbf_book = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic,  100, 90, True)
			elif name == "04.png":
				pxbf_music = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic, 100, 90, True)
			elif name == "parent.png":
				pxbf_parent = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic, 95, 95, True)
			elif name == "kid.png":
				pxbf_kid = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic, 95, 95, True)
			elif name == "book.png":
				pxbf_books = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic, 95, 95, True)
			elif name == "default.png":
				pxbf_default = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic, 144,80, True)
			

		#부모가 보낸 동영상일 경우 uid, 부모 사진, 동영상 제목, null 이미지, 부모인지 아이인지 식별 
		#아이가 저장한 동영상일 경우 uid, null 이미지, 동영상 제목, 아이 사진, 부모인지 아이인지 식별 순으로 list에 추가
		for i in range(len(video)):		 
			self.liststore1.append([video[i]["uid"], pxbf_parent, "    "+video[i]["title"] ,pxbf_null,"parent"])

		for i in range(len(Kvideo)):
			self.liststore1.append([Kvideo[i]["uid"], pxbf_null, space+Kvideo[i]["title"], pxbf_kid, "kid"])

		
		# uid
		uid_text=Gtk.CellRendererText(weight= 50)
		column1_text =Gtk.TreeViewColumn("   ", uid_text, text=0)

		# uid sorting - 오름차순으로 
		self.sort_list = Gtk.TreeModelSort(model=self.liststore1)
		self.sort_list.set_sort_column_id(0,0)
		self.treeview1 = Gtk.TreeView(model=self.sort_list)

		# parent picture 넣을 자리를 만들고 treeview1에 추가 
		parent_pixbuf = Gtk.CellRendererPixbuf()
		column1_pixbuf = Gtk.TreeViewColumn("", parent_pixbuf, pixbuf=1)
		column1_pixbuf.set_alignment(0.5)
		self.treeview1.append_column(column1_pixbuf)
		# video title 넣을 자리를 만들고 treeview1에 추가 
		title_text = Gtk.CellRendererText(weight= 50)
		title_text.set_property('font-desc', font)
		column2_text = Gtk.TreeViewColumn("", title_text, text=2)
		self.treeview1.append_column(column2_text)
		# kid picture 넣을 자리를 만들고 treeview1에 추가 
		kid_pixbuf = Gtk.CellRendererPixbuf()
		column2_pixbuf = Gtk.TreeViewColumn("", kid_pixbuf, pixbuf=3)
		column2_pixbuf.set_alignment(0.5)
		self.treeview1.append_column(column2_pixbuf)
		# "parent or kid 식별" 넣을 자리를 만들고 treeview1에 추가 
		pk_text = Gtk.CellRendererText(weight= 50)
		column3_text = Gtk.TreeViewColumn("", pk_text, text=4)


		# the scrolledwindow - 플리킹 해야 함
		self.scrolled_window1 = Gtk.ScrolledWindow()

		# 스크롤 윈도우를 가로, 세로로 확장하고 간격 5로 지정 
		self.scrolled_window1.set_vexpand(True) 
		self.scrolled_window1.set_hexpand(True)
		self.scrolled_window1.set_border_width(0)

		# scrollbar 가로는 생기지 않도록 Never, 세로는 자동적으로 생기게 함
		# there is always the scrollbar (otherwise: AUTOMATIC - only if needed- or NEVER)
		self.scrolled_window1.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC) 
		self.scrolled_window1.add_with_viewport(self.treeview1)

		#  treeview 처음에 맨 밑의 리스트를 보여주기 위함
		self.treeview1.connect('size-allocate', self.treeview_changed)
		
		# buttonbox 생성 
		buttonbox = Gtk.HButtonBox(Gtk.Orientation.HORIZONTAL)
		buttonbox.set_layout(Gtk.ButtonBoxStyle.SPREAD)
		buttonbox.set_spacing(90)


		# 재생하기 버튼을 클릭하면 video 영상의 뒷배경 보여줌
		# 버튼 이미지 삽입 
		image_play = Gtk.Image()
		image_play.set_from_file("./button/play.png")
		image_play.show()
		b_play = Gtk.Button()
		b_play.add(image_play)
		b_play.show()
		# 버튼 클릭시 on_button_video_background함수로 이동 
		b_play.connect("clicked", self.on_button_video_background)


		# 녹화하기 버튼을 클릭하면 카메라 영상의 뒷배경 보여줌	
		# 버튼 이미지 삽입 
		image_record = Gtk.Image()
		image_record.set_from_file("./button/record.png")
		image_record.show()
		b_record = Gtk.Button()
		b_record.add(image_record)
		b_record.show()
		# 버튼 클릭시 on_button_record_background함수로 이동 
		b_record.connect("clicked", self.on_button_record_background)


		#만들어진 버튼을 buttonbox에 추가
		buttonbox.add(b_play) 
		buttonbox.add(b_record)


		# a grid to attach the widget
		# grid에 스크롤 윈도우와 버튼박스의 위치를 지정해줌 
		grid = Gtk.Grid(row_homogeneous=True, 
						column_spacing=0, row_spacing=2)
		grid.attach(self.scrolled_window1, 0, 0, 7, 4)
		grid.attach(buttonbox, 2,6,1,1 )


		#grid.attach_next_to(buttonbox, self.scrolled_window1,
								# Gtk.PositionType.BOTTOM, , 1)
		
		
		# notebook tab에 message 이미지 
		message = Gtk.Image()
		message.set_from_pixbuf(pxbf_message)
		message.show()
		frame1 = Gtk.Frame()
		frame1.show()
		frame1.add(message)

		# 노트북 page 추가 
		self.notebook.append_page(grid, frame1)

		############################################### Kidsyam : 키즈얌과 연동한 정보들을 리스트 형식으로 보여줌 #############################################
		'''
		#get_kidsyam_information thread start
		kidsyam_thread= threading.Thread(target=self.kidsyam)
		kidsyam_thread.daemon = True
		kidsyam_thread.start()
		'''
		#creating the liststore 
		self.liststore= Gtk.ListStore(GdkPixbuf.Pixbuf, str)

		#kidsyam
		self.kidsyam()

		#creating the treeview
		self.treeview = Gtk.TreeView(model=self.liststore)

		# kid picture
		kid_pixbuf = Gtk.CellRendererPixbuf()
		column_pixbuf = Gtk.TreeViewColumn("", kid_pixbuf, pixbuf=0)
		column_pixbuf.set_alignment(0.5)
		self.treeview.append_column(column_pixbuf)
		# kidsyam information
		inform_text = Gtk.CellRendererText(weight= 50)
		inform_text.set_property('font-desc', font)
		column_text = Gtk.TreeViewColumn("", inform_text, text=1)
		self.treeview.append_column(column_text)


		# the scrolledwindow - 플리킹 안됨!
		scrolled_window2 = Gtk.ScrolledWindow()
		scrolled_window2.set_border_width(0)
		scrolled_window2.set_vexpand(True)
		scrolled_window2.set_hexpand(True)
		# scrollbar 가로는 생기지 않도록 Never, 세로는 자동적으로 생기게 함
		# there is always the scrollbar (otherwise: AUTOMATIC - only if needed- or NEVER)
		scrolled_window2.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC) 
		#scrolled_window2.add_with_viewport(label)
		scrolled_window2.add_with_viewport(self.treeview)
		
		# notebook tab에 kidsyam 이미지 
		kidsyam = Gtk.Image()
		kidsyam.set_from_pixbuf(pxbf_kidsyam)
		kidsyam.show()
		frame2 = Gtk.Frame()
		frame2.show()
		frame2.add(kidsyam)


		# 노트북 page 추가 
		self.notebook.append_page(scrolled_window2, frame2)


		################################################# Audio Book : 부모가 녹음한 동화파일 재생  ################################################################

		# 부모가 보낸 동화 녹음 파일 가져옴 
		u3 = urllib.urlopen(url_book)
		audio = json.loads(u3.read())

		# creating the liststore (parent Audio)
		self.liststore2 = Gtk.ListStore(int, GdkPixbuf.Pixbuf, str)


		for i in range(len(audio)):
			self.liststore2.append([audio[i]["uid"], pxbf_books, '    '+audio[i]["regdate"]])

	
		# creating the treeview
		self.treeview2 = Gtk.TreeView(model=self.liststore2)

		# uid
		uid2_text=Gtk.CellRendererText(weight= 50)
		column1 =Gtk.TreeViewColumn("   ", uid2_text, text=0)

		# uid sorting
		sort= Gtk.TreeModelSort(model=self.liststore2)
		sort.set_sort_column_id(0,0)
		self.treeview2 = Gtk.TreeView(model=sort)
		# book picture
		book_pixbuf = Gtk.CellRendererPixbuf()
		col_pixbuf = Gtk.TreeViewColumn("", book_pixbuf, pixbuf=1)
		col_pixbuf.set_alignment(0.5)
		self.treeview2.append_column(col_pixbuf)

		# book title
		renderer_text = Gtk.CellRendererText()
		renderer_text.set_property('font-desc', font)
		column2 = Gtk.TreeViewColumn("", renderer_text, text=2)
		self.treeview2.append_column(column2)

		# buttonbox
		buttonbox = Gtk.HButtonBox(Gtk.Orientation.HORIZONTAL)
		buttonbox.set_layout(Gtk.ButtonBoxStyle.SPREAD)
		buttonbox.set_spacing(30)

		# 재생하기 버튼을 클릭하면 video 영상의 뒷배경 보여줌
		# 버튼 이미지 삽입
		image_play = Gtk.Image()
		image_play.set_from_file("./button/play.png")
		image_play.show()
		b_play = Gtk.Button()
		b_play.add(image_play)
		b_play.show()
		# 버튼을 클릭하면 on_button_book_background 함수 실행
		b_play.connect("clicked", self.on_button_book_background)
		
		#buttonbox에 버튼 추가 
		buttonbox.add(b_play)

		scrolled_window3 = Gtk.ScrolledWindow()
		scrolled_window3.set_vexpand(True)
		scrolled_window3.set_hexpand(True)
		scrolled_window3.set_border_width(0)
		# there is always the scrollbar (otherwise: AUTOMATIC - only if needed- or NEVER)
		scrolled_window3.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC) 

		# add the image to the scrolledwindow
		scrolled_window3.add_with_viewport(self.treeview2)

		# a grid to attach the widget
		grid = Gtk.Grid(row_homogeneous=True,
						column_spacing=0, row_spacing=2)
		grid.attach(scrolled_window3, 0, 0, 7, 4)
		#grid.attach(b_play, 2,6,1,1 )
		grid.attach(buttonbox, 3,6,1,1 )
		
		# notebook tab에 book 이미지 
		book = Gtk.Image()
		book.set_from_pixbuf(pxbf_book)
		book.show()
		frame3 = Gtk.Frame()
		frame3.show()
		frame3.add(book)

		self.notebook.append_page(grid, frame3)
		

		#########################  Music  #################################
		#self.liststore3 = Gtk.ListStore(GdkPixbuf.Pixbuf, str)
		self.liststore3 = Gtk.ListStore(str)

		# music file 가져오기 
		for root, dirs, files in os.walk('./music/'):
			for file in files:		
				#self.liststore3.append([pxbf_book,file])
				self.liststore3.append([file])

		# creating the treeview
		self.treeview3 = Gtk.TreeView(model=self.liststore3)
		music_text = Gtk.CellRendererText()
		music_text.set_property('font-desc', font)
		column_text = Gtk.TreeViewColumn("", music_text, text=0)
		self.treeview3.append_column(column_text)
	
		# buttonbox
		buttonbox = Gtk.HButtonBox(Gtk.Orientation.HORIZONTAL)
		buttonbox.set_layout(Gtk.ButtonBoxStyle.SPREAD)
		buttonbox.set_spacing(30)

		image_play = Gtk.Image()
		image_play.set_from_file("./button/play.png")
		image_play.show()
		b_play = Gtk.Button()
		b_play.add(image_play)
		b_play.show()
		b_play.connect("clicked", self.on_button_music_background)

		
		buttonbox.add(b_play)

		scrolled_window4 = Gtk.ScrolledWindow()
		scrolled_window4.set_vexpand(True)
		scrolled_window4.set_hexpand(True)
		scrolled_window4.set_border_width(0)
		scrolled_window4.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC) # there is always the scrollbar (otherwise: AUTOMATIC - only if needed- or NEVER)

		# add the image to the scrolledwindow
		scrolled_window4.add_with_viewport(self.treeview3)

		# a grid to attach the widget
		# grid에 스크롤 윈도우와 버튼박스의 위치를 지정해줌 
		grid = Gtk.Grid(row_homogeneous=True,
						column_spacing=0, row_spacing=2)
		grid.attach(scrolled_window4, 0, 0, 7, 4)
		#grid.attach(b_play, 2,5,1,1 )
		grid.attach(buttonbox, 3,6,1,1 )
		#grid.attach_next_to(b_music, scrolled_window4,
		#						 Gtk.PositionType.BOTTOM, 1, 1)
	

		# notebook tab에 music 이미지 
		music = Gtk.Image()
		music.set_from_pixbuf(pxbf_music)
		music.show()
		frame4 = Gtk.Frame()
		frame4.show()
		frame4.add(music)


		self.notebook.append_page(grid, frame4)

		# rotate
		self.notebook.set_tab_pos((self.notebook.get_tab_pos()+1) %4)
		self.notebook.set_tab_pos((self.notebook.get_tab_pos()+1) %4)
	

	# treeview 맨 밑으로 가있도록 하는 기능
	def treeview_changed(self, widget, event, data=None):
		adj = self.scrolled_window1.get_vadjustment()
		adj.set_value( adj.get_upper() - adj.get_page_size() )
	
	###################################### Video get, play, stop #######################################
	'''
	#parent Video get (thread)
	def parent_Vget(self):
		global video

		while 1:
			before=len(video)
			u = urllib.urlopen(url_parentV)
			video= json.loads(u.read())
			after=len(video)
			if after > before:
				self.liststore1.clear()
				for i in range(after):
					self.liststore1.append([video[i]["title"]])
			time.sleep(1)
	'''
	# Background when video play 
	def on_button_video_background(self, widget):
		self.video_window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
		self.video_window.fullscreen()  
		#self.window.set_default_size(500, 400)
		overlay = Gtk.Overlay()
		self.video_window.add(overlay)
		drawingarea = Gtk.DrawingArea()
		overlay.add(drawingarea)

		buttonbox = Gtk.HButtonBox(Gtk.Orientation.HORIZONTAL)
		buttonbox.set_layout(Gtk.ButtonBoxStyle.START)
		buttonbox.set_spacing(100)

		# back 버튼 클릭 시에 Message 화면으로 나감 
		image_back = Gtk.Image()
		image_back.set_from_file("./button/back.png")
		image_back.show()
		b_back= Gtk.Button()
		b_back.add(image_back)
		b_back.show()
		b_back.connect("clicked", self.on_button_video_back)
		
		# 재생/일시정지 버튼을 클릭 시 영상이 pause, play
		image_resume = Gtk.Image()		
		image_resume.set_from_file("./button/resume.png")
		image_resume.show()
		b_resume = Gtk.Button()
		b_resume.add (image_resume)
		b_resume.connect("clicked", self.on_button_video_play_pause)
	
		buttonbox.add(b_back)
		buttonbox.add(b_resume)

		image_bg = Gtk.Image()		
		image_bg.set_from_file("./button/black.png")
		image_bg.show()
		
		# a grid to attach the widget
		# grid에 스크롤 윈도우와 버튼박스의 위치를 지정해줌 
		grid_bg = Gtk.Grid(row_homogeneous=True, 
						column_spacing=0, row_spacing=1)
		grid_bg.attach(image_bg, 0, 0, 7, 4)
		grid_bg.attach_next_to(buttonbox, image_bg,
								 Gtk.PositionType.BOTTOM, 2, 1)

		# drawingarear 위에 버튼 overlay
		overlay.add_overlay(grid_bg)
		#overlay.add_overlay(b_resume)
		overlay.show_all()
		
		self.video_window.show_all()
		time.sleep(0.5)

		# omxplayer로 동영상을 재생시키는 thread
		play_thread = threading.Thread(target=self.video_play)
		play_thread.daemon = True
		play_thread.start()
				

	# 영상을 재생/일시정지 버튼을 클릭했을 시 pause, play
	def on_button_video_play_pause(self, widget):
		subprocess.call('dbuscontrol.sh pause', shell=True)
	# 동화를 재생/일시정지 버튼을 클릭했을 시 pause, play
	def on_button_book_play_pause(self, widget):
		subprocess.call('dbuscontrol.sh pause', shell=True)
	# 음악을 재생/일시정지 버튼을 클릭했을 시 pause, play
	def on_button_music_play_pause(self, widget):
		subprocess.call('dbuscontrol.sh pause', shell=True)

	# Video player
	def video_play(self):
		# treeview에서 선택한 위치와 정보를 가져옴 
		select_video = self.treeview1.get_selection()
		(model, treeiter) = select_video.get_selected()
		if treeiter != None:
			# 부모인지 아이인지 식별하는 model[treeiter][4] 의 정보가 parent이면 부모, kid이면 아이
			if model[treeiter][4] =="parent":
				# uid가 현재 선택한 리스트의 uid와 같다면 영상 재생
				for i in range(len(video)):
					if model[treeiter][0] == video[i]["uid"]:
						subprocess.call ("omxplayer --win '110, 0, 690, 386' -o local "+ video[i]["path"], shell=True)
			elif model[treeiter][4] =="kid":
				# uid가 현재 선택한 리스트의 uid와 같다면 영상 재생
				for i in range(len(Kvideo)):
					if model[treeiter][0] == Kvideo[i]["uid"]:
						subprocess.call ("omxplayer --win '110, 0, 690, 386' -o local "+ Kvideo[i]["path"], shell=True)

				
			# 영상을 모두 재생한 후 뒷 배경 끄기 
			self.video_window.destroy()


	# back while music play 
	def on_button_music_back(self, widget):
		os.system('dbuscontrol.sh stop')
		#os.system('killall omxplayer.bin')
		self.music_window.destroy()

	#back while book audio play
	def on_button_book_back(self, widget):
		os.system('dbuscontrol.sh stop')
		self.book_window.destroy()

	# back while video play or when video recording end
	def on_button_video_back(self, widget):
		os.system('dbuscontrol.sh stop')
		self.video_window.destroy()
	

	#Camera recording background      
	def on_button_record_background(self, widget):
		self.record_window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
		self.record_window.fullscreen()  

		overlay = Gtk.Overlay()
		self.record_window.add(overlay)
		drawingarea = Gtk.DrawingArea()
		overlay.add(drawingarea)

		buttonbox = Gtk.HButtonBox(Gtk.Orientation.HORIZONTAL)
		buttonbox.set_layout(Gtk.ButtonBoxStyle.SPREAD)
		buttonbox.set_spacing(120)

		# 전송하기 버튼을 누르면 영상이 종료되고 저장
		image_send = Gtk.Image()
		image_send.set_from_file("./button/send.png")
		image_send.show()
		b_send = Gtk.Button()
		b_send.add(image_send)
		b_send.connect("clicked", self.on_button_stop_store)

		# 취소하기 버튼을 누르면 저장이 되지 않고 화면이 꺼짐 
		image_cancel = Gtk.Image()
		image_cancel.set_from_file("./button/cancel.png")
		image_cancel.show()
		b_cancel = Gtk.Button()
		b_cancel.add(image_cancel)
		b_cancel.connect("clicked", self.on_button_cancel)
	
		buttonbox.add(b_send)
		buttonbox.add(b_cancel)

		image_bg = Gtk.Image()		
		image_bg.set_from_file("./button/black.png")
		image_bg.show()
		
		# a grid to attach the widget
		# grid에 검은색 배경이미지와  버튼박스의 위치를 지정해줌 
		grid_bg = Gtk.Grid(row_homogeneous=True, 
						column_spacing=0, row_spacing=1)
		grid_bg.attach(image_bg, 0, 0, 7, 4)
		grid_bg.attach_next_to(buttonbox, image_bg,
								 Gtk.PositionType.BOTTOM, 2, 1)

		# drawingarear 위에 버튼 overlay
		overlay.add_overlay(grid_bg)
		overlay.show_all()
		self.record_window.show_all()


		# 아이의 영상을 녹화하는 thread함수  
		thread()

	#recording cancel
	def on_button_cancel (self, widget):
		global flag
		flag = True 
		self.record_window.destroy()

	# kid recording camera stop and store
	def on_button_stop_store(self, widget):
		global flag
		global Kvideo
		global u2

		#window창과 녹화하는 창 off
		flag = True
		self.record_window.destroy()


		# 아이가 찍은 영상의 파일 이름을 현재 날짜와 시간으로 정함 
		now = datetime.datetime.now()
		filename= now.strftime('%Y-%m-%d_%H:%M:%S')+'.mp4'
		# h264파일을 mp4파일로 변환 
		subprocess.call('avconv -i myaud.wav -vn -ar 44100 -ac 2 -ab 192k -f mp3 myaud.mp3', shell=True)
		subprocess.call('avconv -y -i myvid.h264 -i myaud.mp3 -c:v copy -c:a copy '+filename, shell=True)
	
		subprocess.call('rm myaud.mp3', shell=True)
		subprocess.call('rm myaud.wav', shell=True)

		Creator = 'kid'
		Title = filename
		Message = 'hihi'
		
		# 아이의 영상 서버로 보내기 
		url_kid = 'https://test-api.kidsyam.com:3443/test/upload?Creator='+Creator+'&Title='+Title+'&Message='+Message+'&Type=V'
		ffiles = {'PhotoData': open(filename, 'rb')} 
		r = requests.post(url_kid, files=ffiles)

		# familYam 파일 내부로 아이 동영상 파일을 옮김 
		subprocess.call('mv '+filename +' ./familYam/', shell=True)
			
		# 아이의 동영상 정보를 다시 불러와서 "uid, null 이미지, 동영상파일 이름, kid 이미지, 부모인지 아이인지 식볊""
		# 순서로 list에 추가  
		u2 = urllib.urlopen(url_kidV)
		Kvideo = json.loads(u2.read())
		self.liststore1.append([Kvideo[len(Kvideo)-1]["uid"], pxbf_null, space +filename , pxbf_kid, "kid"])
				
		'''
				for root, dirs, files in os.walk('./familYam/'):
					for Kid_video in files:
						if Kid_video == filename:
							self.liststore1.append([pxbf, Kid_video, "kid"])
		'''		

#####################################parent Audio get, record, play, stop / Music #######################################
####################################이 부분도 위에 처럼 수정해야하는 부분~~~

	'''
	#parent Audio get (thread)
	def parent_Aget(self):
		global audio
		# parent Audio url
		while 1:
			before=len(audio)
			u = urllib.urlopen(url_book)
			audio= json.loads(u.read())
			after=len(audio)
			if after > before:
				self.liststore3.clear()
				for i in range(after):
					self.liststore3.append([audio[i]["title"]])
			time.sleep(1)	
	'''
	# parent Audio player
	def book_play(self):
		# treeview에서 선택한 위치와 정보를 가져옴 
		select_book= self.treeview2.get_selection()
		model, treeiter = select_book.get_selected()
		if treeiter != None:
			for i in range(len(audio)):
				# 동화 음성 파일의 uid가 현재 선택한 것과 같으면 음성 재생 
				if model[treeiter][0] == audio[i]["uid"]:
					os.system ('omxplayer -o local '+ audio[i]["path"])
		# 동화가 모두 재생되면 창 끔 
		self.book_window.destroy()



	def on_button_book_background (self, widget):
		self.book_window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
		self.book_window.fullscreen()  
		#self.window.set_default_size(500, 400)
		overlay = Gtk.Overlay()
		self.book_window.add(overlay)
		drawingarea = Gtk.DrawingArea()
		overlay.add(drawingarea)

		buttonbox = Gtk.HButtonBox(Gtk.Orientation.HORIZONTAL)
		buttonbox.set_layout(Gtk.ButtonBoxStyle.START)
		buttonbox.set_spacing(100)

		# back 버튼 클릭 시에 Message 화면으로 나감 
		image_back = Gtk.Image()
		image_back.set_from_file("./button/back.png")
		image_back.show()
		b_back= Gtk.Button()
		b_back.add(image_back)
		b_back.show()
		b_back.connect("clicked", self.on_button_book_back)
		
		# 재생/일시정지 버튼을 클릭 시 영상이 pause, play
		image_resume = Gtk.Image()		
		image_resume.set_from_file("./button/resume.png")
		image_resume.show()
		b_resume = Gtk.Button()
		b_resume.add (image_resume)
		b_resume.connect("clicked", self.on_button_book_play_pause)
	
		buttonbox.add(b_back)
		buttonbox.add(b_resume)

		image_bg = Gtk.Image()		
		image_bg.set_from_file("./button/book.png")
		image_bg.show()
		
		# a grid to attach the widget																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																											
		# grid에 배경 이미지와 버튼박스의 위치를 지정해줌 
		grid_bg = Gtk.Grid(row_homogeneous=True, 
						column_spacing=0, row_spacing=0)
		grid_bg.attach(image_bg, 0, 0, 7, 4)
		grid_bg.attach_next_to(buttonbox, image_bg,
								 Gtk.PositionType.BOTTOM, 2, 1)

		# drawingarear 위에 버튼 overlay
		overlay.add_overlay(grid_bg)
		overlay.show_all()
		
		self.book_window.show_all()


		play_thread = threading.Thread(target=self.book_play)
		play_thread.daemon = True
		play_thread.start()


	def on_button_music_background(self, widget):
		self.music_window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
		self.music_window.fullscreen()  
		#self.window.set_default_size(500, 400)
		overlay = Gtk.Overlay()
		self.music_window.add(overlay)
		drawingarea = Gtk.DrawingArea()
		overlay.add(drawingarea)

		buttonbox = Gtk.HButtonBox(Gtk.Orientation.HORIZONTAL)
		buttonbox.set_layout(Gtk.ButtonBoxStyle.START)
		buttonbox.set_spacing(100)

		# back 버튼 클릭 시에 Message 화면으로 나감 
		image_back = Gtk.Image()
		image_back.set_from_file("./button/back.png")
		image_back.show()
		b_back= Gtk.Button()
		b_back.add(image_back)
		b_back.show()
		b_back.connect("clicked", self.on_button_music_back)
		
		# 재생/일시정지 버튼을 클릭 시 영상이 pause, play
		image_resume = Gtk.Image()		
		image_resume.set_from_file("./button/resume.png")
		image_resume.show()
		b_resume = Gtk.Button()
		b_resume.add (image_resume)
		b_resume.connect("clicked", self.on_button_music_play_pause)
	
		buttonbox.add(b_back)
		buttonbox.add(b_resume)

		image_bg = Gtk.Image()		
		image_bg.set_from_file("./button/music.png")
		image_bg.show()
		
		# a grid to attach the widget																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																											
		# grid에 배경 이미지와 버튼박스의 위치를 지정해줌 
		grid_bg = Gtk.Grid(row_homogeneous=True, 
						column_spacing=0, row_spacing=1)
		grid_bg.attach(image_bg, 0, 0, 7, 4)
		grid_bg.attach_next_to(buttonbox, image_bg,
								 Gtk.PositionType.BOTTOM, 2, 1)

		# drawingarear 위에 버튼 overlay
		overlay.add_overlay(grid_bg)
		overlay.show_all()
		
		self.music_window.show_all()


		play_thread = threading.Thread(target=self.music_play)
		play_thread.daemon = True
		play_thread.start()


	def music_play(self):

		# treeview에서 선택한 위치와 정보를 가져옴 
		select_music= self.treeview3.get_selection()
		(model, treeiter) = select_music.get_selected()
		if treeiter != None:
			for root, dirs, files in os.walk('./music/'):
				for file in files:
					if file == model[treeiter][0]:
						os.system ('omxplayer -o local ./music/'+file)

		self.music_window.destroy()




############################################## kisyam information 가져오고 list에 추가하기   ##############################################
	def kidsyam(self):
		#global string

		#pass login
		#아이의 정보를 가져온다.
		url_login = 'https://api.kidsyam.com/common/GetLoginInfo?Id=gorao_ri@naver.com&Pw=tmddus4141'
		r_login = requests.get(url_login, headers=headers)
		cookies = dict(PHPSESSID=r_login.cookies['PHPSESSID'])
		data_login = json.loads(r_login.text)

		#string = ""
			
		# 아이의 수만큼 for문이 돌아감 
		for i in range(len(data_login["KidsInfo"])):

			# 아이의 이름 
			name = data_login["KidsInfo"][i]["Name"]
			pics_list=[]
			pics_name=[]

			if data_login["KidsInfo"][i]["MapIdxList"] == None:
				string = '    name: '+name+'\n    no sticker'
				if data_login["KidsInfo"][i]["PhotoUrl"] == None:
					self.liststore.append([pxbf_default, string])
				else :
					photo = data_login["KidsInfo"][i]["PhotoUrl"]
					url_photo='http://img.kidsyam.com/PhotoData/'+ photo
					file_name = "%d.jpg" %i
					urllib.urlretrieve(url_photo, file_name)
					subprocess.call('mv '+file_name+' ./kids/', shell=True)
					# get image
					for root, dirs, files in os.walk('./kids/'):
						for fn in files:
							pics_name.append(fn)
							f= os.path.join(root, fn)
							pics_list.append(f)
		
					# image파일 내부의 사진을 각각 해당하는 pixbuf에 넣음 
					for name, pic in zip (pics_name,pics_list):
						if name == file_name:
							pxbf_photo = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic,144,80,True)
							self.liststore.append([pxbf_photo, string])
					subprocess.call('rm ./kids/'+file_name, shell=True)
			else:

				#      , 로 parsing
				listlist = data_login["KidsInfo"][i]["MapIdxList"].split(',')
				mapIdx = listlist[len(listlist)-1]

				# get map information
				# 아이의 정보 중 mapid로 map의 정보를 가져옴 
				url_map = 'https://api.kidsyam.com/common/GetMapUseInfo?MapIdx='+mapIdx
				r_map = requests.get(url_map, headers=headers, cookies=cookies)
				data_map = json.loads(r_map.text)


				# 스티커판 크기 
				total = data_map["MapInfo"]["SlotCnt"]
				# 붙인 스티커 갯수 
				sticker = len(data_map["StickerUseInfo"])
				# 남은 스티커 갯수 
				remainder = total-sticker
				
				string = '    name: ' +name+ '\n    sticker: ' +str(sticker) +',   remainder: ' +str(remainder) 
				
				if data_login["KidsInfo"][i]["PhotoUrl"] == None:
					self.liststore.append([pxbf_default, string])
				else:
					photo = data_login["KidsInfo"][i]["PhotoUrl"]
					url_photo='http://img.kidsyam.com/PhotoData/'+ photo
					file_name = "%d.jpg" %i
					urllib.urlretrieve(url_photo, file_name)
					subprocess.call('mv '+file_name+' ./kids/', shell=True)
					# get image
					for root, dirs, files in os.walk('./kids/'):
						for fn in files:
							pics_name.append(fn)
							f= os.path.join(root, fn)
							pics_list.append(f)
		
					# image파일 내부의 사진을 각각 해당하는 pixbuf에 넣음 
					for name, pic in zip (pics_name,pics_list):
						if name == file_name:
							pxbf_photo = GdkPixbuf.Pixbuf.new_from_file_at_scale(pic,144,80,True)
							self.liststore.append([pxbf_photo, string])
					subprocess.call('rm ./kids/'+file_name, shell=True)
			

#kid video recording and stroe
def Camera():
	global loop
	global flag
	global Min

	#os.system(raspivid -t 300000 -w 1280 -h 720 -b 2000000 -fps 30 -n -awb fluorescent -sa -10 -br 60 -co 50 -o v.h264 | arecord -D plughw:1,0  a.wav)

	with picamera.PiCamera() as camera:
		camera.resolution = (720, 480)
		camera.framerate = 25
		start = datetime.datetime.now()
		#녹화를 시작하면서 화면을 보여줌 
		camera.start_preview(fullscreen=False, window = (110, 0, 580, 386))
		camera.start_recording('./myvid.h264')
		os.system('arecord -D plughw:1,0 myaud.wav')
		while loop:
			print flag # 이 print문이 있어야 동작하기 때문에 없애면 안 됨! 
					   # 외부에서 동작을 무언가 해야 카메라가 녹화를 지속함

			# 5분이 되었을 때와 저장하기 버튼을 눌렀을 때 녹화를 종료
			if flag  or Min == 5: 
				camera.stop_recording()
				camera.stop_preview()
				flag=False
				loop = 0    
			end = datetime.datetime.now()
			timedif = end - start
			Min = timedif.seconds/60
		
		loop =1

def Voice():
	#os.system('arecord -D plughw:1,0 myaud.wav')
	while loop:
		if flag  or Min == 5: 
			subprocess.call('killall arecord', shell=True)

#camera thread
def thread():
	voice_thread = threading.Thread(target=Voice, args=())
	voice_thread.daemon = True
	voice_thread.start()

	cam_thread = threading.Thread(target=Camera, args=())
	cam_thread.daemon = True
	cam_thread.start()



def main(argv):
	def gtk_style():
		css = """
* {
	transition-property: color, background-color, border-color, background-image, padding, border-width;
	transition-duration: 1s;

}

GtkButton {
  background-color: transparent;

  border-color: transparent;
  -moz-border-radius: 40px;
  -webkit-border-radius: 40px;
  border-radius: 40px;

}

GtkNotebook {
	background-color: #b41318;
}
GtkNotebook tab {
	background-color: transparent;
	border-width: 0;
}
GtkNotebook tab:active {
	background-color: transparent;
}
GtkGrid {
	background-color: #21b4ea;
}
GtkFrame{
	border-color: transparent;
	border-width: 0;
}
		"""
		style_provider = Gtk.CssProvider()
		style_provider.load_from_data(css)

		Gtk.StyleContext.add_provider_for_screen(
			Gdk.Screen.get_default(),
			style_provider,
			Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
		)


	gtk_style()

	win = MyWindow()
	win.connect("delete-event", Gtk.main_quit)
	win.show_all()
	Gtk.main()


if __name__ == "__main__":
	main(sys.argv)

